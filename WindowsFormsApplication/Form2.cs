﻿using System;
using Logic;
using System.Windows.Forms;

namespace WindowsFormsApplication
{
    public partial class Form2 : Form
    {
        int vegIndex = -1;

        public Form2()
        {
            InitializeComponent();
            comboBox1.SelectedIndex = 0;
        }
        
        public Form2(int vegIndex)
        {
            this.vegIndex = vegIndex;
            InitializeComponent();
            switch (Form1.salad.Sostav[vegIndex].CyrillicName)
            {
                case "Огурцы":
                    comboBox1.SelectedIndex = 0;
                    break;
                case "Помидоры":
                    comboBox1.SelectedIndex = 1;
                    break;
                case "Картофель":
                    comboBox1.SelectedIndex = 2;
                    break;
                case "Фасоль":
                    comboBox1.SelectedIndex = 3;
                    break;
                case "Горох":
                    comboBox1.SelectedIndex = 4;
                    break;
            }
            if(Form1.salad.Sostav[vegIndex].Weight > 10)
                numericUpDown1.Value = (decimal)Form1.salad.Sostav[vegIndex].Weight;
            else
                numericUpDown1.Value = 10;
            numericUpDown2.Value = Form1.salad.Sostav[vegIndex].ExpirationDays;
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex > 1)
            {
                radioButton1.Enabled = false;
                radioButton2.Enabled = false;
                radioButton1.Checked = true;
            }
            else
            {
                radioButton1.Enabled = true;
                radioButton2.Enabled = true;
                radioButton1.Checked = true;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (vegIndex != -1)
            {
                Form1.salad.Sostav.RemoveAt(vegIndex);
            }

            string vegetable;
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    vegetable = "о";
                    break;
                case 1:
                    vegetable = "п";
                    break;
                case 2:
                    vegetable = "к";
                    break;
                case 3:
                    vegetable = "ф";
                    break;
                case 4:
                    vegetable = "г";
                    break;
                default:
                    vegetable = "о";
                    break;
            }

            bool salty = true;
            if (radioButton2.Checked == true)
                salty = false;
            int weight = (int)numericUpDown1.Value;
            int expDays = (int)numericUpDown2.Value;

            SaladMaker saladMaker = new SaladMaker();
            saladMaker.AddVegetable(Form1.salad, vegetable, weight, salty, expDays);
            this.Close();
        }
    }
}
