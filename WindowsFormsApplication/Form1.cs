﻿using System;
using System.Windows.Forms;
using Logic;
using lab3;

namespace WindowsFormsApplication
{
    public partial class Form1 : Form
    {
        public static Salad salad = SaladMaker.CreateSalad();

        public Form1()
        {
            InitializeComponent();            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Owner = this;
            form2.ShowDialog();
            updateLabels();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBox1.DataSource = salad.Sostav;
            listBox1.DisplayMember = "CyrillicName";
            listBox1.ValueMember = "Weight";
        }

        private void delete_Click(object sender, EventArgs e)
        {
            if(listBox1.SelectedIndex != -1)
                salad.Sostav.RemoveAt(listBox1.SelectedIndex);
            updateLabels();     
        }

        private void edit_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
                MessageBox.Show("Необходимо добавить хотя бы один ингридиент!");
            else
            {
                Form2 form2 = new Form2(listBox1.SelectedIndex);
                form2.Owner = this;
                form2.ShowDialog();
                updateLabels();
            }            
        }

        private void updateLabels()
        {
            label1.Text = "Итого: " + SaladMaker.GetPrice(salad).ToString("N2") + " руб.";
            label2.Text = "Вес: " + SaladMaker.GetWeight(salad) + " гр.";
            label3.Text = "Калорийность: "
                + SaladMaker.GetCalories(salad)
                + " ккал ("
                + SaladMaker.GetCaloriesPer100(salad)
                + " ккал/100 гр.)";
            label4.Text = "Срок годности: " + SaladMaker.GetExpirationDays(salad) + " дней";
            if (SaladMaker.GetExpirationDays(salad) == 2147483647)
                label4.Text = "Срок годности: 0 дней";
            if (SaladMaker.GetCaloriesPer100(salad) == -2147483648)
                label3.Text = "Калорийность: 0 ккал (0 ккал/100 гр.)";
        }

        private void listBox1_Format(object sender, ListControlConvertEventArgs e)
        {
            string cyrillicName = ((AbstractVegetable)e.ListItem).CyrillicName.ToString();
            string weight = ((AbstractVegetable)e.ListItem).Weight.ToString();
            e.Value = cyrillicName + " - " + weight + " гр.";
        }
    }
}
