﻿namespace lab3
{
    public class Pea : Bobs
    {
        public Pea()
        {
            Price = 132;
            Calories = 73;
            CyrillicName = "Горох";
        }
    }
}
