﻿namespace lab3
{
    public abstract class AbstractVegetable
    {
        private int _calories;//калорийность на 100 гр продукта
        private double _weight;//в граммах
        private double _price;//за кг продукта
        private int _expirationDays;
        private string _cyrillicName;
        public bool isWashed;

        public AbstractVegetable()
        {
            isWashed = false;
        }

        public int Calories
        {
            get { return _calories; }
            set { _calories = value; }
        }

        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public int ExpirationDays
        {
            get { return _expirationDays; }
            set { _expirationDays = value; }
        }

        public string CyrillicName
        {
            get { return _cyrillicName; }
            set { _cyrillicName = value; }
        }

        public void Wash()
        {
            isWashed = true;
        }
    }
}
