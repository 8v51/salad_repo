﻿namespace lab3
{
    public class Bobs : AbstractVegetable
    {
        private int _hardness;
        private int _size;

        public int Hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }

        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }
    }
}
