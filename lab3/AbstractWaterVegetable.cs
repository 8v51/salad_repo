﻿namespace lab3
{
    public class AbstractWaterVegetable : AbstractVegetable, CuttableVegetable
    {
        private bool _salty;
        private double _waterPercentage;
                

        public bool Salty
        {
            get { return _salty; }
            set { _salty = value; }
        }

        public double WaterPercentage
        {
            get { return _waterPercentage; }
            set { _waterPercentage = value; }
        }

        public virtual void Cut()
        {

        }
    }
}
