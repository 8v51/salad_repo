﻿namespace lab3
{
    public class Tomato : AbstractWaterVegetable
    {
        public Tomato(bool salty)
        {
            Price = 82;
            Salty = salty;
            if (Salty)
                Calories = 32;
            else
                Calories = 19;
            CyrillicName = "Помидоры";
        }

        public override void Cut()
        {
            //Weight *= 0.95;
        }
    }
}