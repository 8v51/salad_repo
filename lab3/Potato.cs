﻿namespace lab3
{
    public class Potato : AbstractVegetable, CuttableVegetable
    {
        public Potato()
        {
            Price = 40;
            Calories = 82;
            CyrillicName = "Картофель";
        }

        public void Cut()
        {
            //Weight *= 0.99;
        }
    }
}
