﻿namespace lab3
{
    interface CuttableVegetable
    {
        void Cut();
    }
}
