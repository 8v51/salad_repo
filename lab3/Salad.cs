﻿using System.Collections.Generic;
using System.ComponentModel;

namespace lab3
{
    public class Salad
    {
        private double _price;
        private string _name;
        private BindingList<AbstractVegetable> _sostav = new BindingList<AbstractVegetable>();

        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        public BindingList<AbstractVegetable> Sostav
        {
            get { return _sostav; }
            set { _sostav = value; }
        }
                
    }    
}
