﻿namespace lab3
{
    public class Cucumber : AbstractWaterVegetable
    {
        public Cucumber(bool salty)
        {
            Price = 75;
            Salty = salty;
            if (Salty)
                Calories = 11;
            else
                Calories = 14;
            CyrillicName = "Огурцы";
        }

        public override void Cut()
        {
            //Weight *= 0.9;
        }
    }
}
