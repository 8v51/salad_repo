﻿using lab3;
using Logic;
using System;

namespace Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(50, 50);
            Salad salad = SaladMaker.CreateSalad();
            IngridientsChooser chooser = new IngridientsChooser();
            chooser.Choose(salad);
            chooser.AskForName(salad);
            chooser.ShowSalad(salad);
            Console.ReadLine();
        }
    }
}
