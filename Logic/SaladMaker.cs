﻿using lab3;

namespace Logic
{
    public class SaladMaker
    {
        public static Salad CreateSalad()
        {
            Salad salad = new Salad();
            salad.Price = 0;
            salad.Name = "Новый салат";
            return salad;
        }

        public Salad AddVegetable(Salad salad, string vegetable, int weight, bool salty, int expDays)
        {
            switch (vegetable)
            {
                case "п":
                    Tomato tomato = new Tomato(true);
                    if (!salty)
                        tomato.Salty = false;
                    tomato.Weight = weight;
                    tomato.ExpirationDays = expDays;
                    tomato.Wash();
                    tomato.Cut();
                    salad.Sostav.Add(tomato);
                    break;
                case "ф":
                    Beans beans = new Beans();
                    beans.Weight = weight;
                    beans.ExpirationDays = expDays;
                    beans.Wash();
                    salad.Sostav.Add(beans);
                    break;
                case "о":
                    Cucumber cucumber = new Cucumber(true);
                    if (!salty)
                        cucumber.Salty = false;
                    cucumber.Weight = weight;
                    cucumber.ExpirationDays = expDays;
                    cucumber.Wash();
                    cucumber.Cut();
                    salad.Sostav.Add(cucumber);
                    break;
                case "г":
                    Pea pea = new Pea();
                    pea.Weight = weight;
                    pea.ExpirationDays = expDays;
                    pea.Wash();
                    salad.Sostav.Add(pea);
                    break;
                case "к":
                    Potato potato = new Potato();
                    potato.Weight = weight;
                    potato.ExpirationDays = expDays;
                    potato.Wash();
                    potato.Cut();
                    salad.Sostav.Add(potato);
                    break;
            }
            return salad;
        }

        public static int GetCalories(Salad salad)
        {
            double calories = 0;
            foreach(AbstractVegetable veg in salad.Sostav)
            {
                calories += (veg.Calories * veg.Weight / 100);
                //делим на 100 т.к. калорийность задается на 100 г. продукта
            }
            return (int)calories;
        }
        public static int GetCaloriesPer100(Salad salad)
        {
            double calories = GetCalories(salad);
            calories = calories / (GetWeight(salad) / 100);
            return (int)calories;
        }

        public static double GetPrice(Salad salad)
        {
            double price = 0;
            foreach(AbstractVegetable veg in salad.Sostav)
            {
                price += (veg.Price / 1000 * veg.Weight);
                //делим на 1000 т.к. цена за кг продукта
            }
            return price;
        }

        public static int GetExpirationDays (Salad salad)
        {
            int days = 2147483647;
            foreach (AbstractVegetable veg in salad.Sostav)
            {
                if (veg.ExpirationDays < days)
                    days = veg.ExpirationDays;
            }
            return days;
        }

        public static double GetWeight (Salad salad)
        {
            double weight = 0;
            foreach (AbstractVegetable veg in salad.Sostav)
            {
                weight += veg.Weight;
            }
            return weight;
        }
        public static void ChangeName (Salad salad, string name)
        {
            salad.Name = name;
        }
    }
}
