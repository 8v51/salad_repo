﻿using lab3;
using Logic;
using System;

namespace Interface
{
    public class IngridientsChooser
    {
        public void Choose (Salad salad)
        {
            Console.WriteLine("Выберите овощ для добавления");
            Console.WriteLine("Огурец [о]\nПомидор [п]\nКартофель [к]\nФасоль [ф]\nГорох [г]");
            string vegLetter = Console.ReadKey().KeyChar.ToString();
            while (!(vegLetter == "о" || vegLetter == "п" || vegLetter == "к" || vegLetter == "ф" || vegLetter == "г"))
            {
                Console.WriteLine("\nНекорректный ввод, введите снова");
                vegLetter = Console.ReadKey().KeyChar.ToString();
            }

            bool salty = false;
            if(vegLetter == "о" || vegLetter == "п")
            {
                Console.WriteLine("\n\nСвежий или сОленый? [с/о]");
                string saltyLetter = Console.ReadKey().KeyChar.ToString();
                if (saltyLetter == "с")
                    salty = true;
            }
            Console.WriteLine("\n\nСколько грамм?");
            int weight = 0;
            while (weight == 0)            {
                string stringWeight = Console.ReadLine();
                
                if (stringWeight != "")
                    weight = Convert.ToInt32(stringWeight, 10);
                else
                    Console.WriteLine("Некорректный ввод");
            }
            Console.WriteLine("\n\nСколько дней осталось в сроке годности?");
            int expDays = Int32.Parse(Console.ReadLine());

            SaladMaker saladMaker = new SaladMaker();
            saladMaker.AddVegetable(salad, vegLetter, weight, salty, expDays);
            Console.WriteLine("\nДобавить еще ингридиент? [д/н]");
            string addLetter = Console.ReadKey().KeyChar.ToString();
            if (addLetter == "д")
            {
                Console.WriteLine("\n");
                Choose(salad);
            }         
        }

        public void AskForName (Salad salad)
        {
            string saladName;
            Console.WriteLine("\n\nВведите имя салата:");
            saladName = Console.ReadLine();
            SaladMaker.ChangeName(salad, saladName);
        }
        public void ShowSalad (Salad salad)
        {
            Console.WriteLine("\nПоздравляем, салат готов!");
            Console.WriteLine("\nНазвание салата: " + salad.Name);
            Console.WriteLine("Вес салата: " + SaladMaker.GetWeight(salad).ToString("N2") + " гр.");
            Console.WriteLine("Калорийность порции: " + SaladMaker.GetCalories(salad).ToString() + " ккал ("
                + SaladMaker.GetCaloriesPer100(salad).ToString() + " ккал/100 гр)");
            Console.WriteLine("Себестоимость: " + SaladMaker.GetPrice(salad).ToString("N2") + " руб.");
            Console.WriteLine("Салат годен еще " + SaladMaker.GetExpirationDays(salad).ToString() + " дней");
            Console.WriteLine("Состав:");
            foreach (AbstractVegetable veg in salad.Sostav)
            {
                Console.WriteLine("--- " + veg.CyrillicName + " - " + veg.Weight + " гр.");
            }
        }
    }
}
